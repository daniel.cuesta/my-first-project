package com.tanuki.cicdtest

import com.tanuki.cicdtest.viewmodel.Logic
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    private val logic = Logic()

    @Test
    fun addition_isCorrect() {
        var res = logic.sum(2, 2)

        assertEquals(4, res)

        res = logic.sum("2", "2")

        assertEquals(4, res)
    }
}
