package com.tanuki.cicdtest.viewmodel

class Logic {

    fun sum(v1: Int, v2: Int): Int = v1 + v2
    fun sum(v1: String, v2: String): Int = sum(v1.toInt(), v2.toInt())

}