package com.tanuki.cicdtest.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.tanuki.cicdtest.R
import com.tanuki.cicdtest.viewmodel.Logic
import kotlinx.android.synthetic.main.activity_main.*
import net.hockeyapp.android.CrashManager
import net.hockeyapp.android.UpdateManager



class MainActivity : AppCompatActivity() {

    val logic = Logic()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bResult.setOnClickListener { onResultClick() }

        checkForUpdates()
    }

    override fun onResume() {
        super.onResume()
        checkForCrashes()
    }

    override fun onPause() {
        super.onPause()
        unregisterManagers()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterManagers();
    }

    private fun checkForCrashes() {
        CrashManager.register(this)
    }

    private fun checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this)
    }

    private fun unregisterManagers() {
        UpdateManager.unregister()
    }

    private fun onResultClick() {
        val r = logic.sum(etValue1.text.toString(), etValue2.text.toString())

        tvResult.text = " = $r"
    }
}
